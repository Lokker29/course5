from lxml import etree


def main():
    xml_doc = etree.parse('output.xml')
    xml_schema = etree.XMLSchema(etree.parse('output.xsd'))

    try:
        xml_schema.assertValid(xml_doc)
    except etree.DocumentInvalid:
        print("List of errors:\r\n", xml_schema.error_log)
    else:
        print('SUCCESS')


if __name__ == '__main__':
    main()
