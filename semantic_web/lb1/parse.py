import requests
from bs4 import BeautifulSoup
from lxml import etree, html


def get_content(url):
    response = requests.get(url)
    return response.content.decode('utf-8')


def parse_products(soup):
    return soup.find_all('div', attrs={'class': 'product-card'})


def push_to_xml_document(html_document):
    with open("output.xml", 'wb') as out:
        out.write(etree.tostring(html_document))


def main(url):
    content = get_content(url)
    soup = BeautifulSoup(content, 'lxml')

    products = parse_products(soup)
    push_to_xml_document(html.fromstring(str(products)))


if __name__ == '__main__':
    WEB_URL = 'https://allo.ua/ua/products/notebooks/proizvoditel-apple/'

    main(WEB_URL)
